;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (println (apply zarp (readExpr)))
)

(define (zarp i)
	(define (calc a b c i)
	 (if (< i 3) 
	 	a
	 	(calc (- (+ a (* 2 b)) c) a b (- i 1)) 
	 	)	
	)
		(if (< i 3)
		i
		(calc 2 1 0 i)
		)	
)
