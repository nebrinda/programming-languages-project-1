;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (define args (readExpr))
        (define t (getElement args 2))
        (define i (apply resistance args))
		(println i " " (mred i t) " " (mgreen i t) " " (mblue i t))
	)


(define pi  3.14159265358979323846)
		
(define (resistance x y t)

;		(define (printAll i t)
;		(println i " " (mred i t) " " (mgreen i t) " " (mblue i t)))

	(define (calc r s i)
		(if (or (> (+ (^ r 2) (^ s 2)) 4) (= t i));diverges	
			i;number of iterations
			(calc (+ (- (^ r 2) (^ s 2)) x) (+ (* 2 r s) y) (+ i 1))
			)
		)
	(if (> (+ (^ x 2) (^ y 2)) 4);diverges immediately
		0
		(calc 0.0 0.0 0);;this is the returned value from resistance, the number of iterations or t.
	)


)

(define (mred i t)
 		(if (= i t)
 			0
 			(integer (+ (* 255 (cos (* (/ pi 2.0) (/ i (real (- t 1)))))) .5))
	) 		
)

(define (mgreen i t)
	(if (= i t)
		0
		(integer (+ (* 255 (sin (* pi (/ i (real (- t 1)))))) .5))
	)
)

(define (mblue i t)
	(if (= i t)
		0
		(integer (+ (* 255 (sin (* (/ pi 2.0) (/ i (real (- t 1)))))) .5))
	)
) 