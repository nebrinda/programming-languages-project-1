;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (println (apply pt (readExpr)))
        )
	
 (define (pt point)
	(define (show row col spaces first)
		(if (<= row point)
			(if (<= col row)
				(begin
					(if (= first 1);if first value, print full set of spaces
						(printSpaces spaces))
					(if (<= (+ col 1) row);
					(print (calcValue row col)" ")
					(print (calcValue row col))
					)
					(show row (+ col 1) spaces 0)
						)
			(begin
			(if (<= (+ row 1) point)		
			(print "\n"))
			(show (+ row 1) 1 (- spaces 1) 1)))		
		nil))
			
	(define (calcValue row col)
		 (if (or (= col 1) (= col row))
			1 
			(+ (calcValue (- row 1) (- col 1)) (calcValue (- row 1) col)))
			)
	(define (printSpaces num_spaces)
		(inspect num_spaces)
		(pause)
		(if (<= num_spaces 0);;
			(begin
				(print " ")
				(printSpaces (- num_spaces 1))
			)
		)
	)
	(inspect point)
	(pause)
		(if (<= point 0)
		nil
		(show 1 1 point 1)
		)
)
 
		   
