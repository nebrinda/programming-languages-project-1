 ;Written by Nathan Brinda
  (define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (define args (readExpr))
        (println "half of " (car args) " is " (halve (car args)))
        (println "half of " (cadr args) " is " (halve (cadr args)))
        (println (car args) " squared is " (square (car args)))
        (println (cadr args) " squared is " (square (cadr args)))
        (println (apply babyl args))
)


(define (babyl num1 num2)
	(define (babyl-calc n1 n2 partial_sum)
		(halve (- (- (square(+ num1 num2)) (square num1)) (square num2)))  
		)
	
		(halve (- (- (square(+ num1 num2)) (square num1)) (square num2)))  
)


(define (square num)
	(define (square-iter n partial_sum)
		(if (= n 1)
		partial_sum
		(square-iter (- n 1) (- (+ partial_sum n n) 1)))
		)
		(if (= num 0)
		0
		(square-iter num 1))
)



(define (halve num)
	(define (halve-iter n doubled_num previous_double resultant)
		(if (<= n 1)
			resultant
			(if (> (+ doubled_num doubled_num) n)
				(halve-iter (- n doubled_num) 2 1 (+ previous_double resultant))
				(halve-iter n (+ doubled_num doubled_num) (+ previous_double previous_double) resultant)
			)	
		)
	)
	(if (> num 0)
		(halve-iter num 2 1 0)
		0
		)
)