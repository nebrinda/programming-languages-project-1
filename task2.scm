;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (println (apply minMaxSum (readExpr)))
        )
		
(define (minMaxSum x y z)

	(if (< x z)
		(if (< y x)
			(+ z y)
			(if (< y z)
				(+ x z)
				(+ x y)
			)
		)
		(if (< y z)
			(+ x y)
			(if (< y x)
				(+ x z)
				(+ y z)
			)
		)
	)
)