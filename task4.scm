;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (println (apply root12 (readExpr)))
        )

(define (root12 n)
	(define (good-enough? guess)
		(if (<= (abs (- guess (+ (real (/ (* 11.0 guess) 12.0)) (real (/ n (* 12.0 (^ guess 11.0)))) ))) .00000000001)	
			guess ;close enough
			(good-enough? (+ (real (/ (* 11.0 guess) 12.0)) (real (/ n (* 12.0 (^ guess 11.0))))));else recurse
			)
		)		
	(if (< n 0.0)
		nil
		(if (= n 0.0)
			(real n)
			(good-enough? 1.0)
		)		
	)
)