;Written by Nathan Brinda
(define (main)
        (setPort (open (getElement ScamArgs 1) 'read))
        (define args (readExpr));
        (println "ramanujan returns "(ramanujan (getElement args 0)))
        (println "iramanujan returns "(ramanujan (getElement args 0)))
        (println "$3$")
    	)

(define (ramanujan n)
	(define (recursive n i)
		(if (<= n 0)
			1.0
			(+ (^ (+ 1.0 (* (+ i 1.0) (recursive (- n 1) (+ i 1)))) .5))
		)
	)
	(if (<= n 0)
		0.0
		(recursive n 1)
	)
)

(define (iramanujan n)
	(define (iterative n partial_sum)
		(if (<= n 0)
		partial_sum
		(iterative (- n 1) (^ (+ 1 (* (+ n 1) partial_sum)) .5))
		)
	)
	(if (<= n 0)
		0.0
		(iterative n 1)
	)
)
